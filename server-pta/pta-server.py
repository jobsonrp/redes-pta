# Aluno: Jobson Rocha Pereira
# BSI - Redes - Pratica2 - NAT
# Professor: Glauco Goncalves

# -*- coding: cp1252 -*-
import os
import sys
from socket import *

def connection(ip,port):
    global cnt
    cnt = 0
    clientSocket = socket(AF_INET, SOCK_STREAM)
    clientSocket.connect((ip,port))
    return clientSocket

if len(sys.argv) <= 2:
    print("Usage: pta-server.py <r-ip> <r-port>")
    sys.exit(2)
rIp = sys.argv[1]
rPort = int(sys.argv[2])

while 1:
    print "Servidor pronto para receber mensagens. Digite Ctrl+C para terminar."
    try:
        #Cria um socket para tratar a conexao do R
        connectionSocket = connection(rIp,rPort)
        
        #Lista de comando aceitos
        comandosAceitos = ["CUMP","LIST","TERM","PEGA"]

        #Lista de usuarios
        usuarios = ["Maria","Joao","Jose"]
        
        caminhoPastaArquivos = os.getcwd() + "/arquivos"
        listArquivos = os.listdir(caminhoPastaArquivos)

        textoComando = connectionSocket.recvfrom(2048)
        comando = textoComando[0].split(" ")
        print "Comando Recebido --> %s" % comando

        if (not comando[1] in comandosAceitos):
            connectionSocket.send(comando[0] + " NOK")
            print "Resposta --> COMANDO INVALIDO NOK"       

        elif (comando[1] == "CUMP" and len(comando) == 3):
            if (comando[2] in usuarios):
                connectionSocket.send(comando[0] + " OK")
                print "Resposta --> CUMP OK"
                while 1:
                    textoComando = connectionSocket.recvfrom(2048)
                    comando = textoComando[0].split(" ")
                    if (comando[1] == "TERM" and len(comando) == 2):
                        print "Comando Recebido --> %s" % comando
                        connectionSocket.send(comando[0] + " OK")
                        print "Resposta --> TERM OK"
                        connectionSocket.close()
                        break
                    elif (comando[1] == "LIST" and len(comando) == 2):
                        print "Comando Recebido List --> %s" % comando
                        while 1:
                            #print("Lista dos Arquivos === %s" % listArquivos)
                            if listArquivos:                        
                                connectionSocket.send(comando[0] + " ARQS " + str(len(listArquivos)) + " " + ','.join(listArquivos) )
                                print "Resposta --> LIST OK"
                            else:
                                connectionSocket.send(comando[0] + " NOK")
                                print "Resposta --> LIST NOK"
                            break                        
    
                    elif (comando[1] == "PEGA" and len(comando) == 3):
                        print "Comando Recebido --> %s" % comando
                        while 1:
                            arquivoSolicitado = "nenhum"
                            if type(comando[2]) is str:
                                arquivoSolicitado = comando[2].split(",")[0]
                            elif type(comando[2]) is list:
                                arquivoSolicitado = comando[2][0]
                            if (listArquivos and arquivoSolicitado in listArquivos):
                               caminhoArqSolicitado = caminhoPastaArquivos + "/" + comando[2]
                               tamanho = os.path.getsize(caminhoArqSolicitado)                   
                               connectionSocket.send( comando[0] + " ARQ " + str(tamanho) + " " + open(caminhoArqSolicitado,'rb').read() )
                               print "Resposta --> PEGA OK"
                            else:
                                connectionSocket.send(comando[0] + " NOK")
                                print "Resposta --> PEGA NOK"                        
                            break
                    else:
                        connectionSocket.send(comando[0] + " NOK")
                        connectionSocket.close()
                        print "Resposta --> Comando Invalido NOK"                           
            else:
                connectionSocket.send(comando[0] + " NOK")
                connectionSocket.close()
                print "Resposta --> CUMP NOK - Usuario Invalido"

    except (KeyboardInterrupt, SystemExit, error):
        break

connectionSocket.close()
print 'Socket fechado.'
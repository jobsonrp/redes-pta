# Aluno: Jobson Rocha Pereira
# BSI - Redes - Pratica2 - NAT
# Professor: Glauco Goncalves

# -*- coding: cp1252 -*-
import os
from socket import *

clientPort = 12000
#Cria o Socket TCP (SOCK_STREAM) para rede IPv4 (AF_INET)
clientSocket = socket(AF_INET,SOCK_STREAM)
clientSocket.bind(('',clientPort))
#Socket fica ouvindo conexoes. O valor 1 indica que uma conexao pode ficar na fila
clientSocket.listen(1)

serverPort = 12800
#Cria o Socket TCP (SOCK_STREAM) para rede IPv4 (AF_INET)
serverSocket = socket(AF_INET,SOCK_STREAM)
serverSocket.bind(('',serverPort))
#Socket fica ouvindo conexoes. O valor 1 indica que uma conexao pode ficar na fila
serverSocket.listen(1)

numeroConexao = 1

while 1:
    print "R pronto para receber mensagens do Client e Server."
    print "Digite Ctrl+C para terminar."
    comandoC = ['0']
    try:
        while 1:
            if comandoC[0] == '0':
                #Cria um socket para tratar a conexao do Client
                connectionSocketClient, addrClient = clientSocket.accept()
                #Cria um socket para tratar a conexao do Server
                connectionSocketServer, addrServer = serverSocket.accept()
                print "===================== CONEXAO %d =====================" % numeroConexao
                print "Client - Ip: %s || Port: %d" % (addrClient[0],addrClient[1])
                print "Server - Ip: %s || Port: %d" % (addrServer[0],addrServer[1])
                numeroConexao += 1
    
            textoComandoC = connectionSocketClient.recv(2048)
            if textoComandoC != "":
                comandoC = textoComandoC.split(" ")
                print "Comando Recebido do Client -->",comandoC
                print "Resposta enviada ao Server -->",textoComandoC
                connectionSocketServer.send(textoComandoC)
            
            textoComandoS = connectionSocketServer.recv(2048)
            print 'len(textoComandoS) ===== ',len(textoComandoS)
            tamanhoRecebido = len(textoComandoS)
            if textoComandoS != "":
                comandoS = textoComandoS.split(" ")
                print "Comando Recebido do Server -->",comandoS
                if comandoS[0] == '2' and len(comandoS) >= 3:
                    if comandoS[1] == 'ARQ':
                        leituraS = '1'
                        tamanhoArquivo = int(comandoS[2])
                        while tamanhoRecebido < tamanhoArquivo:
                            leituraS = ""
                            print 'entrou aqui'
                            leituraS = connectionSocketServer.recv(2048)
                            textoComandoS += leituraS
                            print "leituraS --- ",leituraS
                            print "textoComandoS --- ",textoComandoS
                            tamanhoRecebido += len(leituraS)
                print "Comando Recebido do Server -->",comandoS
                print "Resposta enviada ao Client -->",textoComandoS  
                connectionSocketClient.send(textoComandoS)
            
            if textoComandoC != "" and textoComandoS != "":
                if comandoC[1] == "CUMP" and comandoS[1] == "OK":
                    comandoC[0] = 'CUMP OK - Manter conexao'
                if comandoC[1] == "TERM" and comandoS[1] == "OK":
                    break
                
            
    except (KeyboardInterrupt, SystemExit):
        break

serverSocket.close()
clientSocket.close()